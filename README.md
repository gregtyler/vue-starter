# Getting started
This is a starter kit for Vue, with Vuex, vue-router, Dexie and rollup.

Either download or clone the contents of this repo. If cloning, be sure to
change the remote URL before pushing changes back up:
```sh
git remote set-url origin {YOUR_URL}
```

Don't forget to change the project name in `package.json` and
`package-lock.json`.